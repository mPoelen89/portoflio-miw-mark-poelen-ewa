DROP USER if exists 'testuser'@'localhost';
flush privileges;

CREATE USER 'testuser'@'localhost' IDENTIFIED BY 'test';
GRANT ALL PRIVILEGES ON cryptobank . * TO 'testuser'@'localhost';

DROP SCHEMA if exists `portfolioopdracht` ;
CREATE SCHEMA IF NOT EXISTS `portfolioopdracht` DEFAULT CHARACTER SET utf8 ;
USE `portfolioopdracht` ;

CREATE TABLE IF NOT EXISTS `portfolioopdracht`.`gebruiker` (
  `voornaam` VARCHAR(45) NOT NULL,
  `achternaam` VARCHAR(45) NOT NULL,
  `wachtwoord` VARCHAR(90) NOT NULL,
  `mail` VARCHAR(45) NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `mail_unique_index` (`mail` ASC));

INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Jan', 'Jansen', '1234', '1@h.nl', '1');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Klaas', 'Klaasen', '1234', '2@h.nl', '2');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Piet', 'Pietersen', '1234', '3@h.nl', '3');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Marie', 'Paris', '1234', '4@h.nl', '4');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Willem', 'Willemsen', '1234', '5@h.nl', '5');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Mark,fiets', 'Poelen', 'Unkown', 'mark@h.nl', '6');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Mark', 'Poelen', 'fiets', 'mark@han.nl', '7');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Peter', 'Jacobs', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'P@h.nl', '8');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Johan', 'Vis', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'j@v.nl', '9');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Hans', 'Teeuwen', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'H@t.nl', '10');
INSERT INTO `portfolioopdracht`.`gebruiker` (`voornaam`,`achternaam`,`wachtwoord`,`mail`,`id`) VALUES ('Hans', 'Teeuwen', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', 'r@t.nl', '12');










