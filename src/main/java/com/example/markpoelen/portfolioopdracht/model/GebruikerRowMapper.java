package com.example.markpoelen.portfolioopdracht.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GebruikerRowMapper implements RowMapper<Gebruiker> {
    @Override
    public Gebruiker mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Gebruiker(resultSet.getLong("id"),
                resultSet.getString("voornaam"),
                resultSet.getString("achternaam"),
                resultSet.getString("wachtwoord"),
                resultSet.getString("mail"));
    }
}
