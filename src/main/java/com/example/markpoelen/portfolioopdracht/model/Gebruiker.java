package com.example.markpoelen.portfolioopdracht.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Gebruiker {
    private static Long ID_COUNT = 0L;
    private Long id;
    private String voornaam;
    private String achternaam;
    private String wachtwoord;
    private String mail;// je zou deze een default waarde van now() kunnen geven
    // Nodige setters, getters, constructoren en toString

    public Gebruiker(Long id, String voornaam, String achternaam, String wachtwoord, String mail) {
        this.id = id;
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.wachtwoord = wachtwoord;
        this.mail = mail;
    }

    public Gebruiker(String voornaam, String achternaam, String wachtwoord, String mail) {
        this(0L, voornaam, achternaam, wachtwoord, mail);
    }

    public Gebruiker() throws ParseException {
        this("Unkown", "Unkown", "Unkown", "Unkown");
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return "Gerbuiker{" +
                "id=" + id +
                ", voornaam='" + voornaam + '\'' +
                ", achternaam='" + achternaam + '\'' +
                ", Geboortedatum=" + wachtwoord +
                ", mail='" + mail + '\'' +
                '}';
    }
}
