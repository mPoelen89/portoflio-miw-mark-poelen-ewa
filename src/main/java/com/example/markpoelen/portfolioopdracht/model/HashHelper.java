package com.example.markpoelen.portfolioopdracht.model;

import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class HashHelper {
    public static String hash(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes(StandardCharsets.UTF_8));

        byte[] digest = md.digest();

        String hashedString = ByteArrayToHexHelper.encodeHexString(digest);

        return hashedString;
    }

}
