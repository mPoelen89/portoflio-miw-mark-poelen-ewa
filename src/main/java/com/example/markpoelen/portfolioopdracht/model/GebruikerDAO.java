package com.example.markpoelen.portfolioopdracht.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.util.List;

@Component
public class GebruikerDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Gebruiker> gebruikerLijst() {
        return jdbcTemplate.query("select * from gebruiker", new GebruikerRowMapper(), null);
    }

    public Gebruiker getOneById(long id) {
        return jdbcTemplate.queryForObject("select * from gebruiker where id=?", new GebruikerRowMapper(), id);
    }

    public Gebruiker getOneByMail(String mail) {
        return jdbcTemplate.queryForObject("select * from gebruiker where mail=?", new GebruikerRowMapper(), mail);
    }

    public Gebruiker getOneByNames(String voornaam, String achternaam) {
        return jdbcTemplate.queryForObject("select * from gebruiker where voornaam=? AND achternaam=?", new GebruikerRowMapper(), voornaam, achternaam);
    }

    public void updateOne(Gebruiker gebruiker, long id) {
        jdbcTemplate.update("update gebruiker set voornaam = ?, achternaam = ?, wachtwoord = ?, mail = ?  where id =?", gebruiker.getVoornaam(), gebruiker.getAchternaam(), gebruiker.getWachtwoord(), gebruiker.getMail(), id);
        System.out.println(getOneById(id));
    }

    public void deleteOneById(long id) {
        jdbcTemplate.update("delete from gebruiker where id = ?", id);
    }

    public long storeOne(Gebruiker gebruiker) {
        String sql = "insert into gebruiker (voornaam, achternaam, wachtwoord, mail) values (?,?,?,?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
            ps.setString(1, gebruiker.getVoornaam());
            ps.setString(2, gebruiker.getAchternaam());
            ps.setString(3, gebruiker.getWachtwoord());
            ps.setString(4, gebruiker.getMail());
            return ps;
        }, keyHolder);
        return keyHolder.getKey().longValue();
    }
}
