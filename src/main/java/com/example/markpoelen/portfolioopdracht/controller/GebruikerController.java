package com.example.markpoelen.portfolioopdracht.controller;

import com.example.markpoelen.portfolioopdracht.model.Gebruiker;
import com.example.markpoelen.portfolioopdracht.model.GebruikerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class GebruikerController {

    @Value("$(posts.url)") // Wat betekent dit?
    private String postUrl;

    @Autowired
    private GebruikerDAO gebruikerDAO;

    @GetMapping
    List<Gebruiker> list() {
        return gebruikerDAO.gebruikerLijst();
    }

    @GetMapping(value = "/{id}")
    public Gebruiker getUserById(@PathVariable("id") long id) {
        Gebruiker user = gebruikerDAO.getOneById(id);
        if (!user.equals(null)) {
            return user;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found!");
        }
    }

    @GetMapping(value = "/{voornaam}/{achternaam}")
    public Gebruiker getUserById(@PathVariable("voornaam") String voornaam, @PathVariable("achternaam") String achternaam) {
        Gebruiker user = gebruikerDAO.getOneByNames(voornaam, achternaam);
        if (!user.equals(null)) {
            return user;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found!");
        }
    }

    @PostMapping(value = "/create")
    ResponseEntity<?> createUser(@RequestBody Gebruiker user) {
        System.out.println("Create user methode uitgevoerd");
        gebruikerDAO.storeOne(user);
        return ResponseEntity.ok("gelukt");
    }


}
