package com.example.markpoelen.portfolioopdracht.controller;

import com.example.markpoelen.portfolioopdracht.model.Gebruiker;
import com.example.markpoelen.portfolioopdracht.model.GebruikerDAO;
import com.example.markpoelen.portfolioopdracht.model.HashHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@Controller
public class LoginController {

    @Autowired
    GebruikerDAO gebruikerDAO;

    @Value("$(posts.url)") // Wat betekent dit?
    private String postUrl;

    @GetMapping(value = "")
    public String login(Model model) throws ParseException {
        model.addAttribute("gebruiker", new Gebruiker());
        return "login";
    }

    @PostMapping(value = "/home")
    public String index (@ModelAttribute Gebruiker gebruiker, Model model) throws NoSuchAlgorithmException {
        model.addAttribute("gebruiker", gebruiker);
        Gebruiker dbGebruiker = gebruikerDAO.getOneByMail(gebruiker.getMail());
        if(dbGebruiker.getWachtwoord().equals(HashHelper.hash(gebruiker.getWachtwoord()))){
            return "index";
        } else {
            return "login";
        }


    }
}
