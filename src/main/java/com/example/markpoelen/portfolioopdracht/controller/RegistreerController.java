package com.example.markpoelen.portfolioopdracht.controller;

import com.example.markpoelen.portfolioopdracht.model.Gebruiker;
import com.example.markpoelen.portfolioopdracht.model.GebruikerDAO;
import com.example.markpoelen.portfolioopdracht.model.HashHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@Controller
public class RegistreerController {

    @Autowired
    GebruikerDAO gebruikerDAO;

    @GetMapping(value = "/registreergebruiker")
    String registreerForm(Model model) throws ParseException {
        model.addAttribute("gebruiker", new Gebruiker());
        return "registreer";
    }

    @PostMapping(value = "/registreergebruiker")
    String registreerSubmit(@ModelAttribute Gebruiker gebruiker, Model model) throws NoSuchAlgorithmException {

        model.addAttribute("gebruiker", gebruiker);
        gebruiker.setWachtwoord(HashHelper.hash(gebruiker.getWachtwoord()));
        gebruikerDAO.storeOne(gebruiker);

        return "login";
    }
}
