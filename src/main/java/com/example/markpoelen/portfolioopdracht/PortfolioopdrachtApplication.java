package com.example.markpoelen.portfolioopdracht;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortfolioopdrachtApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(PortfolioopdrachtApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
