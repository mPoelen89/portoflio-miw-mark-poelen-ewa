CREATE SCHEMA IF NOT EXISTS `portfolioopdracht`;
 DROP TABLE IF EXISTS `portfolioopdracht`.`gebruiker`; # uncomment als je steeds met een verse tabel wil werken en geen duplicate entry exceptions wilt bij het gebruikt van automatische initialisatie bij herstart

CREATE TABLE IF NOT EXISTS `portfolioopdracht`.`gebruiker` (
  `voornaam` VARCHAR(30) NOT NULL,
  `achternaam` VARCHAR(45) NOT NULL,
  `wachtwoord` VARCHAR(66) NOT NULL,
  `mail` VARCHAR(50) not null UNIQUE,
  `id` INT NOT NULL auto_increment,
  PRIMARY KEY (`id`));

insert into `portfolioopdracht`.`gebruiker`
(`id`, `voornaam`, `achternaam`, `wachtwoord`, `mail`)
values
(1, "Jan", "Jansen", '1234', '1@h.nl'),
(2, "Klaas", "Klaasen", '1234', '2@h.nl'),
(3, "Piet", "Pietersen", '1234', '3@h.nl'),
(4, "Marie", "Paris", '1234', '4@h.nl'),
(5, "Willem", "Willemsen", '1234', '5@h.nl');

